<?php

namespace App\Tests;

use App\Service\CustomerService;
use App\Service\ResponseService;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{

	public function testCustomerService()
    {
		$this->assertClassHasAttribute('entityManager', CustomerService::class);
		$this->assertClassHasAttribute('customerRepository', CustomerService::class);
		$this->assertClassHasAttribute('connection', CustomerService::class);
		$this->assertClassHasAttribute('platform', CustomerService::class);
		$this->assertClassHasAttribute('serializer', CustomerService::class);
		$this->assertClassHasAttribute('finder', CustomerService::class);
	}

	public function testResponseService()
	{
		$responseService = new ResponseService();
		$this->assertEquals($responseService::CODE_SUCCESS, $responseService->setSuccessResponse());
		$this->assertEquals($responseService::CODE_ERROR, $responseService->setErrorResponse());
	}
}
