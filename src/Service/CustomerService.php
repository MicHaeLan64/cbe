<?php

namespace App\Service;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Wrapper\CustomerWrapper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CustomerService
{
	// EntityManager
	private $entityManager;

	// CustomerRepository
	private $customerRepository;

	// Database connection
	private $connection;

	// Database platform
	private $platform;

	// Symfony Serializer
	private $serializer;

	// Symfony Finder
	private $finder;

	// Define customer entity
	const CUSTOMER_CLASS = 'App\Entity\Customer';

	// Define gender code
	const MALE_CODE = 1;
	const FEMALE_CODE = 2;

	// Define gender text
	const MALE_TEXT = 'Male';
	const FEMALE_TEXT = 'Female';

	/**
	 * CustomerService constructor.
	 * Inject EntityManagerInterface
	 * Setup database connection, database platform, serializer, and finder
	 * @param EntityManagerInterface $entityManager
	 * @param CustomerRepository $customerRepository
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function __construct(EntityManagerInterface $entityManager, CustomerRepository $customerRepository)
	{
		$this->entityManager = $entityManager;
		$this->customerRepository = $customerRepository;
		$this->connection = $this->entityManager->getConnection();
		$this->platform = $this->connection->getDatabasePlatform();
		$this->serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
		$this->finder = new Finder();
	}

	/**
	 * Import data from csv file
	 * @param $path file path
	 * @param $file file name
	 * @return bool
	 * @throws \Doctrine\DBAL\ConnectionException
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function importData($path, $file)
	{
		$this->connection->beginTransaction();
		try {
			// truncate customer table
			$classMetaData = $this->entityManager->getClassMetadata(self::CUSTOMER_CLASS);
			$truncateSql = $this->platform->getTruncateTableSql($classMetaData->getTableName());
			$this->connection->executeUpdate($truncateSql);

			// Load data from csv file
			$finder = $this->finder;
			$finder->files()->in($path)->name($file);
			foreach ($finder as $file) {
				// decode file content
				$data = $this->serializer->decode($file->getContents(), 'csv');

				// add data to table
				foreach($data as $customer) {
					$this->createCustomer($customer);
				}
				break;
			}
			$this->entityManager->flush();
			$this->connection->commit();
			$res = true;
		} catch (Exception $e) {
			$this->connection->rollBack();
			$res = false;
		}

		return $res;
	}

	/**
	 * Get customer records
	 * @return array
	 */
	public function getAllCustomers()
	{
		$customers = array();
		$collection = $this->customerRepository->findAll();
		foreach ($collection as $customer) {
			$customers[] = $this->wrapCustomer($customer);
		}

		return $customers;
	}

	/**
	 * Get Customer data with gender formatted to text
	 * @param $customer
	 * @return CustomerWrapper
	 */
	private function wrapCustomer($customer)
	{
		$wrapper = new CustomerWrapper();
		$wrapper->id = $customer->getCid();
		$wrapper->firstName = $customer->getFirstName();
		$wrapper->lastName = $customer->getLastName();
		$wrapper->email = $customer->getEmail();
		$wrapper->gender = $customer->getGender() == self::MALE_CODE ? self::MALE_TEXT : self::FEMALE_TEXT;
		$wrapper->ipAddress = $customer->getIpAddress();
		$wrapper->company = $customer->getCompany();
		$wrapper->city = $customer->getCity();
		$wrapper->title = $customer->getTitle();
		$wrapper->website = $customer->getWebsite();
		return $wrapper;
	}

	/**
	 * Create customer record
	 * @param $data
	 */
	private function createCustomer($data)
	{
		// Initialize gender data
		$gender = $data['gender'] == self::MALE_TEXT ? self::MALE_CODE : self::FEMALE_CODE;

		// create customer record
		$customer = new Customer();
		$customer->setCid($data['id']);
		$customer->setFirstName($data['first_name']);
		$customer->setLastName($data['last_name']);
		$customer->setEmail($data['email']);
		$customer->setGender($gender);
		$customer->setIpAddress($data['ip_address']);
		$customer->setCompany($data['company']);
		$customer->setCity($data['city']);
		$customer->setTitle($data['title']);
		$customer->setWebsite($data['website']);

		// save customer record
		$this->entityManager->persist($customer);
	}
}