<?php

namespace App\Service;


class ResponseService
{
	const CODE_SUCCESS = 1;
	const CODE_ERROR = 2;

	public function setSuccessResponse()
	{
		return array(
			'code' => self::CODE_SUCCESS
		);
	}

	public function setErrorResponse()
	{
		return array(
			'code' => self::CODE_ERROR
		);
	}
}