<?php

namespace App\Wrapper;


class CustomerWrapper
{
	public $id;
	public $firstName;
	public $lastName;
	public $email;
	public $gender;
	public $ipAddress;
	public $company;
	public $city;
	public $title;
	public $website;
}