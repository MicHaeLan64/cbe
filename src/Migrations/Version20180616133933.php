<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180616133933 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer CHANGE first_name first_name VARCHAR(255) NOT NULL, CHANGE last_name last_name VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL, CHANGE ip_address ip_address VARCHAR(255) NOT NULL, CHANGE company company VARCHAR(255) NOT NULL, CHANGE city city VARCHAR(255) NOT NULL, CHANGE title title VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer CHANGE first_name first_name VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE last_name last_name VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE email email VARCHAR(60) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE ip_address ip_address VARCHAR(15) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE company company VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE city city VARCHAR(40) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE title title VARCHAR(60) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
