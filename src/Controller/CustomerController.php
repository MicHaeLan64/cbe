<?php

namespace App\Controller;

use App\Service\CustomerService;
use App\Service\ResponseService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CustomerController extends Controller
{
	// CustomerService
	private $customerService;

	// ResponseService
	private $responseService;

	// Define import csv file name
	const FILE_NAME = 'customers.csv';

	/**
	 * CustomerController constructor.
	 * Inject CustomerService and ResponseService.
	 * @param CustomerService $customerService
	 * @param ResponseService $responseService
	 */
	public function __construct(CustomerService $customerService, ResponseService $responseService)
	{
		$this->customerService = $customerService;
		$this->responseService = $responseService;
	}

	/**
	 * Import customer.csv file to database
	 * @Route("/import", name="import_customer", methods = {"GET"})
	 */
    public function importAction()
	{
		// get file path
		$path = $this->get('kernel')->getProjectDir(). '/public/';

		$res = $this->customerService->importData($path, self::FILE_NAME);

		// build response by import result
		if ($res) {
			$response = $this->responseService->setSuccessResponse();
		} else {
			$response = $this->responseService->setErrorResponse();
		}

		return $this->json($response);
	}

	/**
	 * Get all customer data
	 * @Route("/customers", name="get_customers", methods = {"GET"})
	 */
	public function getCustomersAction()
	{
		// retrieve customer data from database
		$customers = $this->customerService->getAllCustomers();

		return $this->json($customers);
	}
}
