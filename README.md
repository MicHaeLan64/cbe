## To Run this code
1. Import database.sql to Mysql database.
2. Change .env file, set database username, password, and database name.
3. Run command *php -S 127.0.0.1:8000 -t public* under project root directory.
4. To import customer data to database, go *127.0.0.1:8000/import*.
5. To get customer data from database, go *127.0.0.1:8000/customers*. 
